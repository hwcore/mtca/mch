#!/usr/bin/env python3
#import configparser
import argparse
import logging
from pathlib import Path
from datetime import datetime

from mchlib import MCH


def parse():
    """e.g.
    $ ./mch.py
    """
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--reg_netbox",
        action="store_true",
        required=False,
        help="Register MCH in Netbox.",
    )
    parser.add_argument(
        "--configure_mch",
        action="store_true",
        required=False,
        help="Run 'golden configuration'.",
    )
    parser.add_argument(
        "--moxa_port",
        type=int,
        # metavar='',
        default=1,
        # choices=list(range(1, 17)),
        required=False,
        help="The moxa port [1..16] to access.",
    )
    parser.add_argument(
        "--moxa_ip",
        type=str,
        required=False,
        default="172.30.5.37",
        help="The moxa ip address.",
    )
    parser.add_argument(
        "--network",
        type=str,
        required=False,
        default="CSLab-GeneralLab",
        help="The operation network vlan.",
    )
    parser.add_argument(
        "--update_fw",
        type=str,
        required=False,
        dest="fw_ver",
        # choices=["2.21.8", "2.22.3", "latest"],
        default="latest",
        help="Update FW.",
    )
    parser.add_argument(
        "--update_fw_force",
        action="store_true",
        required=False,
        help="Force firmware update.",
    )
    parser.add_argument(
        "--connection",
        type=str,
        required=False,
        choices=["SSH", "Telnet"],
        default="Telnet",
        help="How to connect to the MOXA and/or MCH (default: Telnet)."
    )
    parser.add_argument(
        "--token",
        type=str,
        required=False,
        help="netbox token.",
    )
    parser.add_argument(
        "--mch_backplane",
        type=int,
        required=True,
        # metavar='',
        default=3,
        choices=[3, 9],
        help="The mch backplane type.",
    )
    parser.add_argument(
        "--mch_ip",
        type=str,
        required=False,
        # default="127.0.0.1",
        help="The mch ip address.",
    )
    parser.add_argument(
        "--mch_hostname",
        type=str,
        required=False,
        # default="localhost",
        help="The mch hostname.",
    )
    return parser.parse_args()


# create common logger for all modules
def get_logger():
    logger = logging.getLogger(__name__)
    if not Path("logs").is_dir():
        Path("logs").mkdir()
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter("%(asctime)s %(levelname)s %(filename)s:%(lineno)d %(message)s")
    dateandtime = datetime.now().strftime("%Y%m%d_%H%M%S")
    logname = f"./logs/log_{dateandtime}"

    # to console
    h_console = logging.StreamHandler()
    h_console.setLevel(logging.INFO)
    h_console.setFormatter(formatter)
    logger.addHandler(h_console)
    # to file
    h_file = logging.FileHandler(logname)
    h_file.setLevel(logging.INFO)
    h_file.setFormatter(formatter)
    logger.addHandler(h_file)

    logger.info(f"Log file: {logname}")

    return logger, logname


def main(args):
    # do the things for one MCH
    try:
        logger.info("Working on MCH")
        mch = MCH(logger, args)

        # TODO: It manages the IP configurations via MOXA telnet, so if it is not enabled then --mch_ip has to be used instead, it would be good to change it to read the IP from mch for other jobs
        if args.reg_netbox:
            logger.info("job: reg_netbox")
            mch.register_in_netbox()
            logger.info("The MOXA link to the MCH may now be removed.")

        if args.fw_ver is not None:
            logger.info("job: fw_ver")
            mch.update_mch_fw()

        if args.configure_mch:
            logger.info("job: configure_mch")
            mch.configure_mch()

    except Exception as e:
        logger.error(f"Failed for MCH Error: {e}")
        exit(1)


if __name__ == "__main__":
    args = parse()
    logger, logname = get_logger()
    main(args)
