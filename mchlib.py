import os
import re
import time
from collections import OrderedDict

import yaml
from gendev_tools.gendev_interface import ConnType, MoxaConnType
from gendev_tools.nat_mch.nat_mch import NATMCH, BackplaneType

from plugs.netboxHandler import NetboxHandler

# Configuration
CONF_DIR = "conf/"

# Support functions
def IsFwVerLower(version_str, target_str):
    version_str = re.sub(r'[a-zA-Z]', '', version_str)
    target_str = re.sub(r'[a-zA-Z]', '', target_str)
    # Split the version strings into components
    version_components = list(map(int, version_str.split('.')))
    target_components = list(map(int, target_str.split('.')))

    # Compare the components numerically
    for v_comp, t_comp in zip(version_components, target_components):
        if v_comp < t_comp:
            return True
        elif v_comp > t_comp:
            return False

    # If all components are equal, the versions are equal
    return False

def GetFwVerLatest():
    fw_path = os.path.realpath("mch_fw_latest.bin")

    pattern = r'(\d+_\d+_\d+)'
    version = re.search(pattern, fw_path)
    if version:
        version = version.group(1).replace('_', '.')
        return version

    pattern = r'(\d+.\d+.\d+)'
    version = re.search(pattern, fw_path)
    if version:
        return version

    return ""

def NormalizeVersion(version_string):
    # Split the version string by '.' and remove the 'V' prefix
    version_str = re.sub(r'[a-zA-Z]', '', version_string)
    version_parts = version_str.split('.')

    # Join the parts with underscores
    converted_version = '_'.join(version_parts)

    return converted_version

# Core class
class MCH:
    def __init__(
        self,
        logger,
        args
    ):
        self.logger = logger
        self.ip_addr = args.mch_ip
        self.hostname = args.mch_hostname
        self.backplane = args.mch_backplane
        self._args = args

    @property
    def args(self):
        return self._args

    def register_in_netbox(self):
        """Registering the MCH in Netbox.
        This will override whatever hostname and ip address are
        specified in the .ini file.
        If the MCH is already registered in Netbox,
        this step will be omitted and the hostname and ip address
        will simply be fetched from Netbox.
        """
        self.logger.info("Attempting to connect to the MCH using the MOXA backend")
        moxa_ip_addr = self.args.moxa_ip
        moxa_mch_port = self.args.moxa_port
        network = self.args.network
        netbox_token = self.args.token

        if self.args.connection == "Telnet":
            moxa_connection = MoxaConnType.TELNET
        else:
            moxa_connection = MoxaConnType.SSH

        # create GenDev interface using the MOXA connection
        natmch = NATMCH(
            ip_address="",
            allowed_conn=[ConnType.MOXA],
            conn_port=moxa_mch_port,
            conn_ip_address=moxa_ip_addr,
            backplane=BackplaneType(self.backplane),
            moxa_connection=moxa_connection,
            logger=self.logger,
        )

        natmch.device_info()
        self.logger.info("The base information from the MCH is available")

        self.logger.info(f"MCH FW version: {natmch.fw_ver}")
        self.logger.info(f"MCH serial number: {natmch.serial_num}")
        self.logger.info(f"MCH MAC address: {natmch.mac_address}")

        # URL to the Netbox API
        netbox_handler = NetboxHandler(token=netbox_token, logger=self.logger)
        
        self.hostname = f"mch-{natmch.serial_num}"
        isregistered = netbox_handler.check_mac_address(natmch.mac_address)
        if not(isregistered):
            self.logger.info("""Registring MCH in Netbox.""")
            # Netbox takes some time to register new hosts in the network.
            # By default there is a timeout of 1 minute.

            # Ansible groups
            ansible_groups = ""

            self.ip_addr = netbox_handler.register_mch(
                self.hostname, natmch.mac_address, network
            )
            if not self.ip_addr:
                raise Exception("No free IPs for this network")

            self.logger.info(
                f"""MCH registered as the host {self.hostname}
                with IP address: {self.ip_addr}"""
            )
        else:
            self.ip_addr = isregistered[1]
            self.hostname = isregistered[0]
            self.logger.info(
                f"""The MCH ({isregistered[0]}) was registered previously with
                IP address: {isregistered[1]})"""
            )

        self.logger.info("Enabling DHCP")
        natmch._mox_conn.ip_address = self.ip_addr
        natmch._mox_conn.hostname = self.hostname
        natmch._mox_conn.open()
        valid = natmch._mox_conn._enable_dhcp()
        if not valid:
            self.logger.error("Failed to enable DHCP mode")
        else:
            self.logger.info("Successfully enabled DHCP mode")

        if self.args.connection == "SSH":
            self.logger.info("Enabling SSH")
            natmch._mox_conn._enable_ssh()

        self.logger.info("Setting IP address")
        valid = natmch._mox_conn._set_ip_addr()
        if not valid:
            self.logger.error(f"Failed to configure the MCH IP Address {self.ip_addr}")
        else:
            self.logger.info(f"Successfully set MCH IP Address {self.ip_addr}")

        self.logger.info(
            """Checking the MCH using the MOXA backend until the IP address
            is assigned by the DHCP server."""
        )
        registered = False
        while not registered:
            valid, mch_info = natmch.device_info()
            if mch_info["Network"]["ip_address"] == self.ip_addr:
                registered = True
                self.logger.info("The MCH is reachable on the network!")
            else:
                self.logger.warning(
                    """The MCH is not reachable on the network yet,
                    waiting 20 more seconds"""
                )
                time.sleep(20)

    def get_allowed_conn(self):
        if self.args.connection == "SSH":
            allowed_conn=[ConnType.ETHER, ConnType.SSH]
        else:
            allowed_conn=[ConnType.ETHER, ConnType.TELNET]
        return allowed_conn

    def update_mch_fw(self):
        """Update the MCH FW to the version specified
        in the command. This will reboot the MCH.
        """

        fw_ver = self.args.fw_ver
        if fw_ver is None:
            print(f"ERROR fw_ver is None {fw_ver}")
            exit(1)

        if fw_ver == 'latest':
            fw_ver = GetFwVerLatest()
        if fw_ver == "":
            print(f"ERROR fw_ver {fw_ver}")
            exit(1)

        allowed_conn = self.get_allowed_conn()

        # create GenDev interface using the backend connection
        natmch = NATMCH(
            ip_address=self.ip_addr,
            hostname=self.hostname,
            allowed_conn=allowed_conn,
            backplane=BackplaneType(self.backplane),
            logger=self.logger,
        )
        valid, info = natmch.device_info()

        if self.args.update_fw_force or IsFwVerLower(info["Board"]["fw_ver"],fw_ver):
            self.logger.info(f"Updating MCH FW to {fw_ver}")
            success, message = natmch.update_fw(fw_ver)
            if not success:
                self.logger.info(message)
            else:
                valid, info = natmch.device_info()
                if valid:
                    if info["Board"]["fw_ver"] == fw_ver:
                        self.logger.info(f"FW updated to V{fw_ver}.")
                    else:
                        self.logger.error("The FW update failed.")
        else:
            self.logger.info("The MCH FW is up to date.")

    def configure_mch(self) -> object:
        """Configure the MCH with the 'golden configuration'.
        The correct .yaml file will be used depending on if the
        crate is specified as '3u' or '9u'
        """

        allowed_conn = self.get_allowed_conn()

        # create GenDev interface using the backend connection
        natmch = NATMCH(
            ip_address=self.ip_addr,
            hostname=self.hostname,
            allowed_conn=allowed_conn,
            backplane=BackplaneType(self.backplane),
            logger=self.logger,
        )

        self.logger.info("Starting configuration of the MCH.")

        _ , info = natmch.device_info()

        def represent_dictionary_order(self, dict_data):
            return self.represent_mapping("tag:yaml.org,2002:map", dict_data.items())

        def setup_yaml():
            yaml.add_representer(OrderedDict, represent_dictionary_order)

        setup_yaml()

        fw_ver = info["Board"]["fw_ver"]
        self.logger.info("Config directory " + CONF_DIR)
        self.logger.info("Firmware version to configure " + fw_ver)
        fw_ver_norm = NormalizeVersion(fw_ver)
        self.logger.info("Normalized firmware version " + fw_ver_norm)
        self.logger.info("Backplane size to configure " + str(self.backplane))

        if self.backplane == 3:
            self.backplane_file = CONF_DIR + "mch_3u_" + fw_ver_norm + ".yaml"
        else:
            self.backplane_file = CONF_DIR + "mch_9u_" + fw_ver_norm + ".yaml"

        if os.path.exists(self.backplane_file) is not True:
            self.logger.info("The MCH backplane file does not exist!!! " + self.backplane_file)
            self.logger.info("Default yaml cofiguration is applied.")
            if self.backplane == 3:
                self.backplane_file = CONF_DIR + "mch_3u_default.yaml"
            else:
                self.backplane_file = CONF_DIR + "mch_9u_default.yaml"

        self.logger.info("The MCH backplane file " + self.backplane_file)

        with open(self.backplane_file, "r") as goldencfg:
            goldencfg = OrderedDict(yaml.safe_load(goldencfg))

        self.logger.info("Applying the configuration to the MCH")
        valid, response = natmch.set_configuration("basecfg", goldencfg, apply=False)
        success = True
        if not valid:
            self.logger.error(
                f"Setting configuration of 'basecfg' failed -> {response}"
            )
            success = False

        valid, response = natmch.set_configuration("pcie", goldencfg, apply=False)
        if not valid:
            self.logger.error(
                f"Setting configuration of 'goldencfg' failed\n -> {response}"
            )
            success = False

        if self.args.connection == "SSH":
            valid, response = natmch._ssh_conn.set_configuration(
            "backplane", goldencfg, apply=True
        )
        else:
            valid, response = natmch._tel_conn.set_configuration(
                "backplane", goldencfg, apply=True
            )
        if not valid:
            self.logger.info(response)
            success = False

        self.logger.info(
            """The MCH has been configured.
            Wait for DHCP server (~2 min) before running check of the settings."""
        )

        # Wait for the DHCP server
        time.sleep(120)
        valid, response = natmch.check_configuration("basecfg", goldencfg)
        if not valid:
            self.logger.warning(response)
            success = False
        else:
            self.logger.info("The base configuration is properly set!")

        valid, response = natmch.check_configuration("pcie", goldencfg)
        if not valid:
            self.logger.warning(response)
            success = False
        else:
            self.logger.info("The PCIe configuration is properly set!")

        if not success:
            self.logger.warning(
                "The MCH was not successfully configured! See error messages above."
            )
        else:
            self.logger.info("MCH successfully updated and configured!")
