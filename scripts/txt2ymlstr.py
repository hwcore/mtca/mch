#!/usr/bin/env python3

import sys

if len(sys.argv) != 3:
    print("Usage: input_file output_file")
    sys.exit(1)

input_file = sys.argv[1]
output_file = sys.argv[2]

with open(input_file, 'r') as txt_file:
    text_content = txt_file.read()

# Replace newlines with the \n escape sequence
text_content = text_content.replace('\n', '\\n')

# Escape double quotes in the content
text_content = text_content.replace('"', r'\"')

with open(output_file, 'w') as out_file:
    out_file.write(text_content)

print(f"Content from {input_file} converted and saved to {output_file}.")
