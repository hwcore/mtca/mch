#!/usr/bin/python3

import yaml
from pprint import pprint
from collections import OrderedDict
from gendev_tools.nat_mch.nat_mch import NATMCH, BackplaneType
from gendev_tools.gendev_interface import ConnType
from os import path
import urllib

# CHANGE THE IP ADDRESS
mch_ips = []
mch_sizes = []

# Latest MCH firmware version
latest_fw_ver = "V2.21.8"

reboot = False

# Set to True, to perform update of configuration, if required
update = True

CONF_DIR = "../conf"

# Again, this trick forces the yaml parser to use OrderedDict objects
# rather than regular dict objects
def represent_dictionary_order(self, dict_data):
    return self.represent_mapping("tag:yaml.org,2002:map", dict_data.items())


def setup_yaml():
    yaml.add_representer(OrderedDict, represent_dictionary_order)


def update_firmware(mch, address, target_fw, cur_fw, reboot, update):
    if target_fw != cur_fw:
        reboot = True
        print("Firmware update required... This will take a few minutes...")
        good, response = mch.update_fw(target_fw[1:])
        if good is False:
            print("Firmware update failed for {}:".format(mch))
            print("    {}".format(response))
        else:
            print("Firmware update succeeded for {}".format(mch))
    elif target_fw == cur_fw:
        print("Firmware is up-to-date ({})".format(cur_fw))

    return reboot


def check_and_update(mch, category, config, reboot, update):

    # First check if we already have a valid config applied
    valid, message = mch.check_configuration(category, config)

    # Update configuration if necessary
    if not valid and update:
        print(message)
        print("\nApplying default configuration...\n")

        # Set golden configuration
        valid, message = mch.set_configuration(category, config, apply=False)
        reboot = True

        if not valid:
            print("Setting {} configuration failed!")
            print(message)

        valid, message = mch.check_configuration(category, config)

        if not valid:
            print(message)
        else:
            print("\nThe {}  configuration is good!".format(category))

    else:
        if valid:
            print("\nThe {}  configuration is good!".format(category))
        else:
            print(
                "\nThe {}  configuration is bad, but ''update'' is set to False.".format(  # noqa: E501
                    category
                )
            )

    return reboot


setup_yaml()

# Once we know the MCH is alive and reachable, let's load the file and
# push it to the MCH:
backplane_files = [CONF_DIR + "mch_3u_golden_backplane.yaml", CONF_DIR + "mch_9u_golden.yaml"]
mch_config_ver = "v0.4"

for backplane_file in backplane_files:
    if not path.exists(backplane_file):
        url = "https://artifactory.esss.lu.se/artifactory/mtca-deployment/config/{}/{}".format(  # noqa: E501
            mch_config_ver, backplane_file
        )
        a = urllib.request.urlretrieve(url, backplane_file)
    else:
        print(
            "Configuration file already exists ({}). Skipping download.".format(  # noqa: E501
                backplane_file
            )
        )

for i in range(0, len(mch_ips)):

    ip_address = mch_ips[i]
    if mch_sizes[i] == 3:
        backplane = BackplaneType.B3U
        backplane_file = backplane_files[0]
    elif mch_sizes[i] == 9:
        backplane = BackplaneType.B9U
        backplane_file = backplane_files[1]

    with open(backplane_file, "r") as configfile:
        config = yaml.safe_load(configfile)

    # Change the IP!
    mch = NATMCH(
        ip_address=ip_address,
        allowed_conn=[ConnType.ETHER, ConnType.TELNET],
        backplane=backplane,
    )

    valid, info = mch.device_info()

    # This will print the main information for the target MCH
    if valid:
        print("MCH Info:")
        pprint(info)

    # Update firmware, if required
    reboot_slots = update_firmware(
        mch, ip_address, latest_fw_ver, info["Board"]["fw_ver"], reboot, update
    )  # noqa: E501

    reboot = check_and_update(mch, "basecfg", config, reboot, update)
    reboot = check_and_update(mch, "pcie", config, reboot, update)

    # If a reboot is required
    if reboot or reboot_slots:
        # Only reboot if a new configuration has been applied
        if reboot:
            mch._reboot(sleep=120)

        # Reboot slots
        mch.reboot_all_slots()
