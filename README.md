# mch_golden_config
Repository to hold the golden reference configuration files. Used by https://gitlab.esss.lu.se/hwcore/mtca/ess-gendev-tools

# From command line
*mch.py* exists to be able to run the configuration from the command line, in cases when it is not possible to use the Jupyterhub script. 

It is possible to
* register the MCH in CSEntry
* update the MCH FW
* apply the base, PCIe and backplane configurations

## Prerequisites

In order to run the configuration script from the command line, you must install the following:

* python3

Python >= 3.7 is required.

And the python modules listed in requirements.txt.

You must configure the EPICS environment before running the test suite.
For the e3 environment, this requires you to source setE3Env.bash.

 ### config_mch_moxa.ini (obsolete)
 The file *config_mch_moxa.ini* must be edited to match the specific MCH.

If **registering the MCH in Netbox** you need to set:
* MoxaIPAddress
* MoxaMCHPort
* MCHBackplane
* Network 
* NetboxToken

An access token to access the Netbox API can be retrieved from https://netbox.esss.lu.se/user/api-tokens/

If **only updating the FW and/or applying the configuration** you need to set:
* MCHBackplane
* MCHIPAddress
* MCHHostname
* MCHFW (skip if not updating the FW)

## Running the script

```
usage: mch.py [-h] [--reg_netbox] [--configure_mch] [--moxa_port MOXA_PORT] [--moxa_ip MOXA_IP] [--network NETWORK] [--update_fw FW_VER] [--update_fw_force]
              [--connection {SSH,Telnet}] [--token TOKEN] --mch_backplane {3,9} [--mch_ip MCH_IP] [--mch_hostname MCH_HOSTNAME]

options:
  -h, --help            show this help message and exit
  --reg_netbox          Register MCH in Netbox.
  --configure_mch       Run 'golden configuration'.
  --moxa_port MOXA_PORT
                        The moxa port [1..16] to access.
  --moxa_ip MOXA_IP     The moxa ip address.
  --network NETWORK     The operation network vlan.
  --update_fw FW_VER    Update FW.
  --update_fw_force     Force firmware update.
  --connection {SSH,Telnet}
                        How to connect to the MOXA and/or MCH (default: Telnet).
  --token TOKEN         netbox token.
  --mch_backplane {3,9}
                        The mch backplane type.
  --mch_ip MCH_IP       The mch ip address.
  --mch_hostname MCH_HOSTNAME
                        The mch hostname.
```


```
# e.g.
./mch.py --configure_mch --update_fw 2.21.8
./mch.py --moxa_ip 172.30.5.36 --moxa_port 3 --mch_backplane 9 --update_fw 2.22.3 --configure_mch --reg_netbox --token XXXXXXXX
./mch.py --mch_ip 172.30.5.xxx --mch_hostname test.cslab.esss.lu.se --mch_backplane 3 --update_fw 2.22.3 --update_fw_force
./mch.py --mch_ip 172.30.5.xxx --mch_hostname test.cslab.esss.lu.se --mch_backplane 3 --update_fw 2.22.3
```

**_NOTE:_** This is **not** possible when registering the MCH in Netbox (using MOXA). 
Using the argument ``--reg_netbox`` will default the number of devices to 1. 
