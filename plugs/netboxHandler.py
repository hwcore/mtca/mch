import logging
import requests
from urllib.parse import urljoin

class NetboxHandler(object):
    def __init__(
        self,
        token=None,
        logger=None,
        url="https://netbox.esss.lu.se/",
    ):
        self.api_url = urljoin(url, "api/")
        self.token = token
        self.session = requests.Session()
        self.session.headers.update({"Authorization": "Token {}".format(self.token)})
        self.logger = logger

    """
    """
    def check_response(self, response):
        if response.content:
            self.logger.debug(str(response.status_code) + ": " + str(response.json()))
        else:
            self.logger.debug(response.status_code)
        return response

    def request(self, method, *args, **kwargs):
        r"""Sends a request using the session

        :param method: HTTP method
        :param \*args: Optional arguments
        :param \*\*kwargs: Optional keyword arguments
        :return: :class:`requests.Response <Response>` object
        """
        r = self.session.request(method, *args, **kwargs)
        return self.check_response(r)


    def get(self, endpoint, **kwargs):
        r"""Sends a GET request to the given endpoint

        :param endpoint: API endpoint
        :param \*\*kwargs: Optional arguments to be sent in the query string
        :return: :class:`requests.Response <Response>` object
        """
        return self.request("GET", urljoin(self.api_url, endpoint), params=kwargs)


    def post(self, endpoint, **kwargs):
        r"""Sends a POST request to the given endpoint

        :param endpoint: API endpoint
        :param \*\*kwargs: Optional arguments to be sent as json data in the body of the :class:`requests.Request`
        :return: json-encoded content of the response
        """
        r = self.request("POST", self.api_url + endpoint, json=kwargs)
        return r.json()

    def patch(self, endpoint, **kwargs):
        r"""Sends a PATCH request to the given endpoint

        :param endpoint: API endpoint
        :param \*\*kwargs: Optional arguments to be sent as json data in the body of the :class:`requests.Request`
        :return: json-encoded content of the response
        """
        r = self.request("PATCH", self.api_url + endpoint, json=kwargs)
        return r.json()

    def check_mac_address(self, mac_address):
        ret = self.get("dcim/interfaces/?mac_address="+mac_address)
        if ret.json()["count"] == 0:
            return False
        else:
            hostname = ret.json()["results"][0]["device"]["name"]
            device_id = ret.json()["results"][0]["device"]["id"]
            ret = self.get("ipam/ip-addresses/?device_id="+str(device_id))
            ip_address = ret.json()["results"][0]["address"]
            ip_address = ip_address.split("/")[0]
            return hostname, ip_address

    def get_device_type_id(self, device_type_name):
        ret = self.get("dcim/device-types/?slug="+device_type_name)
        if ret.json()["count"] > 0:
            return ret.json()["results"][0]["id"]
        else:
            return False

    def get_role_id(self, role_name):
        ret = self.get("dcim/device-roles/?name="+role_name)
        if ret.json()["count"] > 0:
            return ret.json()["results"][0]["id"]
        else:
            return False

    def get_site_id(self, site_name):
        ret = self.get("dcim/sites/?name="+site_name)
        if ret.json()["count"] > 0:
            return ret.json()["results"][0]["id"]
        else:
            return False

    def get_tenant_id(self, tenant_name):
        ret = self.get("tenancy/tenants/?name="+tenant_name)
        if ret.json()["count"] > 0:
            return ret.json()["results"][0]["id"]
        else:
            return False

    def get_tag_id(self, tag_name):
        ret = self.get("extras/tags/?name="+tag_name)
        if ret.json()["count"] > 0:
            return ret.json()["results"][0]["id"]
        else:
            return False

    def get_vlan_id(self, vlan_name):
        ret = self.get("ipam/vlans/?name="+vlan_name)
        if ret.json()["count"] > 0:
            return ret.json()["results"][0]["id"]
        else:
            return False

    def get_prefix(self, vlan_id):
        ret = self.get("ipam/prefixes/?vlan_id="+str(vlan_id))
        if ret.json()["count"] > 0:
            return ret.json()["results"][0]["prefix"]
        else:
            return False

    def get_next_free_ip(self, prefix):
        ret = self.get("ipam/ip-ranges/", parent=prefix)
        if ret.json()["count"] == 0:
            return False
        ip_range_id = ret.json()["results"][0]["id"]
        ret = self.get("ipam/ip-ranges/"+str(ip_range_id)+"/available-ips/")

        # TODO check the result when is full
        if len(ret.json()) > 0:
            return ret.json()[0]["address"]
        else:
            return False

    def get_contact_id(self, name):
        ret = self.get("tenancy/contacts/?name="+name)
        if ret.json()["count"] > 0:
            return ret.json()["results"][0]["id"]
        else:
            return False

    def get_contact_role_id(self, name):
        ret = self.get("tenancy/contact-roles/?name="+name)
        if ret.json()["count"] > 0:
            return ret.json()["results"][0]["id"]
        else:
            return False

    def register_mch(self, hostname, mac_address, network = "CSLab-GeneralLab", description = "", system_owner="fayechicken"):
        # 0. Check if there are available IPs on the network
        vlan_id = self.get_vlan_id(network)
        prefix = self.get_prefix(vlan_id)
        ip_address = self.get_next_free_ip(prefix)
        if not ip_address:
            self.logger.error("No free IPs available for this network")
            return False

        # 1. Register Device
        device_type_id =  self.get_device_type_id("mtca-mch")
        role_id = self.get_role_id("Unknown")
        site_id = self.get_site_id("ESS Main Site")
        tenant_id = self.get_tenant_id("All")
        tags_ids = [self.get_tag_id("all")]
        tags_ids.append(self.get_tag_id("labnetworks"))
        if (self.get_tag_id(network.lower())):
            tags_ids.append(self.get_tag_id(network.lower()))

        response = self.post("dcim/devices/", name = hostname,
            device_type =  device_type_id,
            role = role_id,
            site = site_id,
            description = description,
            status = "active",
            tenant = tenant_id,
            tags = tags_ids
            )
        self.logger.info("Device Registered")
        device_id = response["id"]

        # 2 . Register interface
        response = self.post("dcim/interfaces/", name = hostname,
            device = device_id,
            type = "other",
            description = description,
            mac_address = mac_address,
            mode = "access",
            untagged_vlan = vlan_id,
            enabled = True
            )
        interface_id = response["id"]
        self.logger.info("Interface Registered")

        # 3 . Register IP
        #Add an IP address
        #
        #    Go to the prefix corresponding to the VLAN.
        #    Go to the range in that prefix
        #    Get the next free IP
        #    Status: active
        #    Select the interface with the same name as the device

        response = self.post("ipam/ip-addresses/", address = ip_address,
            status = "active",
            assigned_object_type = "dcim.interface",
            assigned_object_id = interface_id
            )

        ip_address_id = response["id"]
        self.logger.info("IP Registered")

        # 4. Make the IP primary for the device
        response = self.patch("dcim/devices/" + str(device_id) + "/",
            primary_ip4 = ip_address_id
            )

        self.logger.info("Marked IP as primary for the device")

        # 5. Include contact
        system_owner_id = self.get_contact_role_id("system_owner")
        contact_id_ = self.get_contact_id(system_owner)


        response = self.post("tenancy/contact-assignments/", contact = {"id" : contact_id_},
            content_type = "dcim.device",
            object_id = device_id,
            role = {"id" : system_owner_id}
            )

        self.logger.info("Include system_owner contact")
        self.logger.info("Include system_owner contact")

        #6. Set device to active
        response = self.patch("dcim/devices/" + str(device_id) + "/",
            status = "active",
            )
        self.logger.info("Device set to active")

        return ip_address.split("/")[0]
